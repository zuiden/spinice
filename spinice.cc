#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>

using namespace std;

bool p=false;
double d=0.0725;
double l=0.1;
double b=0.01;
double k=10.0;
double Lx=1.0;
double Ly=1.0;

struct spin
{
    double x[2],s[2];
    void pos(double xp,double yp)
    {
        x[0]=xp,x[1]=yp;
    }
    void theta(double x)
    {
        s[0]=cos(x),s[1]=sin(x);
    }
    double x1x()
    {
        return x[0]+0.5*d*s[0];
    }
    double x1y()
    {
        return x[1]+0.5*d*s[1];
    }
    double x2x()
    {
        return x[0]-0.5*d*s[0];
    }
    double x2y()
    {
        return x[1]-0.5*d*s[1];
    }
};

vector<spin> sys;
int I,J;

double delta(double x1,double y1,double x2,double y2)
{
    double dx,dy;
    if(p)
    {
        const double adx=x2-x1,ady=y2-y1;
        dx=fabs(adx)<Lx*0.5?adx:(adx-fabs(adx+Lx*0.5)+fabs(adx-Lx*0.5)),dy=fabs(ady)<Ly*0.5?ady:(ady-fabs(ady+Ly*0.5)+fabs(ady-Ly*0.5));
    }
    else dx=x2-x1,dy=y2-y1;
    return sqrt(pow(dx,2)+pow(dy,2));
}

double Vy(double r)
{
    return b/(r*exp(k*r));
}

double Vij(spin i,spin j)
{
    double ans=0.0;
    ans+=Vy(delta(i.x1x(),i.x1y(),j.x1x(),j.x1y()));
    ans+=Vy(delta(i.x2x(),i.x2y(),j.x2x(),j.x2y()));
    ans+=Vy(delta(i.x2x(),i.x2y(),j.x1x(),j.x1y()));
    ans+=Vy(delta(i.x1x(),i.x1y(),j.x2x(),j.x2y()));
    return ans;
}

void init_triangular_lattice(int N)
{
    sys.clear();
    sys.reserve(N);
    const int kx=((int)round(sqrt(sqrt(3.0)*N/(2.0)))),ky=((int)round(sqrt(N/(2.0*sqrt(3.0)))));
    int i=1,j=0;
    double a=0.0,b=0.0;
    spin s;
    for(int n=0;n<N;n++)
    {
        s.x[0]=a,s.x[1]=b;
        sys.push_back(s);
        if(i>=kx) i=1,b+=sqrt(3.0)*l/2.0,a=((j%2)?0.0:l/2.0),j++;
        else a+=l,i++;
    }
    Lx=kx*l,Ly=ky*l*sqrt(3.0);
    I=sys.size(),J=sys.size();
}

void get_x(double *x,int N)
{
    for(int i=0;i<N;i++) x[i]=sys[i].x[0];
}

void get_y(double *x,int N)
{
    for(int i=0;i<N;i++) x[i]=sys[i].x[1];
}

double V(double *theta,int N)
{
    double ans=0.0;
    for(int i=0;i<N;i++) sys[i].theta(theta[i]);
    for(int i=0;i<I;i++) for(int j=i+1;j<J;j++) ans+=Vij(sys[i],sys[j]);
    return ans;
}
