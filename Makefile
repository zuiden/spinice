CC=g++
spinice: spinice.cc
	swig -Wall -Wextra -c++ -python spinice.i
	$(CC) -Wall -Wextra -Ofast -fPIC -flto -c spinice.cc
	$(CC) -Wall -Wextra -Ofast -fPIC -flto -c spinice_wrap.cxx -I/usr/include/python2.7 -I/usr/lib/python2.7/site-packages/numpy/core/include/
	$(CC) -flto -shared spinice.o spinice_wrap.o -o _spinice.so

clean:
	rm -f spinice.o
	rm -f spinice_wrap.cxx
	rm -f spinice_wrap.o
	rm -f _spinice.so
	rm -f spinice.py
	rm -f *.pyc
