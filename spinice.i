%module spinice
%{
    #define SWIG_FILE_WITH_INIT
    extern bool p;
    extern double d;
    extern double l;
    extern double b;
    extern double k;
    extern double Lx;
    extern double Ly;
    extern void init_triangular_lattice(int N);
    extern double V(double *theta,int N);
    extern double get_x(double *x,int N);
    extern double get_y(double *x,int N);
%}

%include numpy.i
%init %{
import_array();
%}
%apply (double *INPLACE_ARRAY1,int DIM1){(double *theta,int N)};
%apply (double *ARGOUT_ARRAY1,int DIM1){(double *x,int N)};
%apply (double *ARGOUT_ARRAY1,int DIM1){(double *x,int N)};

extern bool p;
extern double d;
extern double l;
extern double b;
extern double k;
extern double Lx;
extern double Ly;
extern void init_triangular_lattice(int N);
extern double V(double *theta,int N);
extern double get_x(double *x,int N);
extern double get_y(double *x,int N);
