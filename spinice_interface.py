#!/usr/bin/env python2
import math
import spinice
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import time

#Nr of particles
N=30
#Toggle periodic boundary conditions
spinice.cvar.p=False
#Dimer diameter
spinice.cvar.d=0.0725
#Lattice constant
spinice.cvar.l=0.1
#Yukawa coupeling constant
spinice.cvar.b=0.01
#Reciprocal Yukawa length
spinice.cvar.k=10
#Box size in x-direction
spinice.cvar.Lx=1.0
#Box size in y-direction
spinice.cvar.Ly=1.0

spinice.init_triangular_lattice(N)

xin=np.random.random_sample((N,)).astype('d')*np.pi
xbounds=np.array([[0.0,np.pi] for i in range(N)])
print(spinice.V(xin))
xout=opt.basinhopping(spinice.V,xin,niter=1000,T=1.0,minimizer_kwargs={'bounds':xbounds}).x
print(spinice.V(xout))

my_dpi=80
plt.figure(figsize=(800/my_dpi,800/my_dpi),dpi=my_dpi)
plt.axis('off')
fig=plt.gcf()
ax0=plt.subplot(111)
ax0.set_xlabel(r'$x$',fontsize=30)
ax0.set_ylabel(r'$y$',fontsize=30)

#Lattice constant
a=spinice.cvar.l
#Dimer radius
delta=spinice.cvar.d/2
#Reciprocal Yukawa length
k=spinice.cvar.k

xt=spinice.get_x(N)[1]
yt=spinice.get_y(N)[1]

xdots=np.ravel([[xt[i]-delta*math.cos(xout[i]),xt[i]+delta*math.cos(xout[i]),None] for i in range(len(xt))])
ydots=np.ravel([[yt[i]-delta*math.sin(xout[i]),yt[i]+delta*math.sin(xout[i]),None] for i in range(len(yt))])

na=np.newaxis
phi=np.linspace(0.0,2*np.pi,100)
x_line=xdots[np.where(xdots)][na,:]+k**-1*np.cos(phi[:,na])
y_line=ydots[np.where(ydots)][na,:]+k**-1*np.sin(phi[:,na])

ax0.scatter(xt,yt,color='#000000',marker='x',s=25,zorder=1)
ax0.plot(x_line,y_line,color='#080808',alpha=0.3,linestyle='--',linewidth=1,zorder=2)
ax0.plot(xdots,ydots,color='#0000FF',linewidth=5,zorder=3)
ax0.scatter(xdots,ydots,color='#00FF00',marker='o',s=300,zorder=4)

ax0.set_aspect('equal', 'datalim')

fig.tight_layout()
fig.savefig('spinice'+time.strftime('-%Y-%j-%H-%M-%S')+'.pdf',dpi=my_dpi)

plt.close()
